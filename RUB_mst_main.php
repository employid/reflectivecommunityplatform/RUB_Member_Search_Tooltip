<?php

/*
 * Plugin Name: RUB Member Search Tooltip
 * Description: The plugin provides a tooltip for the buddypress member search.
 * Version: 0.1
 * Author: Christian Busch
 * License: All rights reserved
 */

/**
 * Loads language files.
 */
function rub_mst_load_textdomain() {
	load_plugin_textdomain( 'RUB_mst_tooltip', false, basename( dirname( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'rub_mst_load_textdomain' );

/**
 * Replaces part of the search form to include the tooltip via title attribute.
 *
 * @param $original_form the
 *        	as provided by BuddPress
 *        	
 * @return the form with the added tooltip
 */
function rub_mst_add_tooltip_filter($original_form) {
	$search = '<label><input';
	$replace = '<label><input data-toggle="popover" data-trigger="focus" data-placement="bottom" data-content="' . __( 'You can search by all profile fields (e.g. names, regions, and job roles).', 'RUB_mst_tooltip' ) . '"';
	
	$search_form_html = str_replace( $search, $replace, $original_form );
	
	return $search_form_html;
}
add_filter( 'bp_directory_members_search_form', 'rub_mst_add_tooltip_filter' );
